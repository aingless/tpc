#!/usr/bin/root.exe -b
{
   TString filename = gApplication->Argv(2);
   TFile * f = TFile::Open(filename, "READ");
   TTree * h101 = (TTree *)f->Get("h101");
   h101->Process("tpc.C+");
   gApplication->Terminate(0);
}
