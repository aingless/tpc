#!/bin/bash

# To set custom output path, set it in OUT_PATH variable, e. g.
# $ OUT_PATH="/data/" ./parallel.sh
# (see tpc.C).

# cat /data/mainz/201903/beam/list_beam.txt | parallel root '-l -b -q go.C\(\"/data/mainz/201903/beam/{}\"\)'
# parallel -j 8 --eta --workdir /home/inglessi/cernbox/p_e/code/root/tpc/2019-03 -a /data/mainz/201903/beam/list_beam.txt root '-l -b -q go.C\(\"/data/mainz/201903/beam/{}\"\)'

WORKDIR="/home/inglessi/cernbox/p_e/code/root/tpc/2019-03"
INLIST="/data/mainz/201903/beam/list_beam.txt"
INDIR="/data/mainz/201903/beam"

# run_root () {
#   root -l -b -q go.C\(\"$INDIR/$1\"\)
# }

# parallel --dry-run -j 8 --eta --workdir $WORKDIR -a $INLIST run_root {}
parallel --eta --workdir $WORKDIR -a $INLIST ./root_ana.sh $INDIR/{}
